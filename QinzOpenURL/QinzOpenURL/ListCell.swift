//
//  ListCell.swift
//  QinzOpenURL
//
//  Created by Qinz on 17/3/22.
//  Copyright © 2017年 FEC. All rights reserved.
//

import UIKit

class ListCell: UITableViewCell {
    
    
    
    @IBOutlet weak var titleLB: UILabel!
    
    
    //MARK: -- 注册Cell
    class func getCell(_ tableView:UITableView) -> ListCell!{
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as? ListCell
        
        if cell == nil{
            cell = Bundle.main.loadNibNamed("ListCell", owner: nil, options: nil)?.first as? ListCell
            cell?.selectionStyle = .none
        }
        return cell!
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
