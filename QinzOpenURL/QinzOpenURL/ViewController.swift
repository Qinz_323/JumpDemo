//
//  ViewController.swift
//  QinzOpenURL
//
//  Created by Qinz on 17/3/22.
//  Copyright © 2017年 FEC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let listArr = ["无线局域网","蓝牙","个人热点","运营商","通知","睡眠","通用","调节亮度","墙纸","声音","siri语音助手","隐私","电话","icloud","iTunes Strore 与 APP Store","safari","关于本机","软件更新","辅助功能","日期与时间","键盘","存储空间","语言与地区","VPN","描述文件与设备管理","音乐","备忘录","照片与相机","还原","Twiter","Facebook"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "跳转至系统各个设置页面"
    }
}

extension ViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    //MARK: -- TableView的代理方法
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    //MARK: -- 每组返回多少个
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return listArr.count
    }
    
    //MARK: -- 每个cell显示的内容
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = ListCell.getCell(tableView)
        
        cell?.titleLB.text = listArr[indexPath.row]
        
        return cell!
    }
    
    //MARK: -- 每个cell的高度
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    
    //MARK: -- 选择每个cell执行的操作
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
        case 0://无线局域网
            jump(url: "App-prefs:root=WIFI")
        case 1: //蓝牙
            jump(url: "App-Prefs:root=Bluetooth")
        case 2: //个人热点
            jump(url: "App-prefs:root=INTERNET_TETHERING")
        case 3: //运营商
            jump(url: "App-prefs:root=Carrier")
        case 4: //通知:需要大写后面加上ID
            jump(url: "App-prefs:root=NOTIFICATIONS_ID")
        case 5: //睡眠
            jump(url: "App-prefs:root=DO_NOT_DISTURB")
        case 6: //通用
            jump(url: "App-prefs:root=General")
        case 7: //调节亮度
            jump(url: "App-prefs:root=DISPLAY&BRIGHTNESS")
        case 8://墙纸
            jump(url: "App-prefs:root=Wallpaper")
        case 9://声音
            jump(url: "App-prefs:root=Sounds")
        case 10://siri语音助手
            jump(url: "App-prefs:root=SIRI")
        case 11://隐私
            jump(url: "App-prefs:root=Privacy")
        case 12: //电话
            jump(url: "App-prefs:root=Phone")
        case 13:  //icloud
            jump(url: "App-prefs:root=CASTLE")
        case 14://iTunes Strore 与 APP Store
            jump(url: "App-prefs:root=STORE")
        case 15://safari
            jump(url: "App-prefs:root=SAFARI")
        case 16: //关于本机
            jump(url: "App-prefs:root=General&path=About")
        case 17://软件更新
            jump(url: "App-prefs:root=General&path=SOFTWARE_UPDATE_LINK")
        case 18: //辅助功能
            jump(url: "App-prefs:root=General&path=ACCESSIBILITY")
        case 19: //日期与时间
            jump(url: "App-prefs:root=General&path=DATE_AND_TIME")
        case 20: //键盘
            jump(url: "App-prefs:root=General&path=Keyboard")
        case 21://存储空间
            jump(url: "App-prefs:root=CASTLE&path=STORAGE_AND_BACKUP")
        case 22: //语言与地区
            jump(url: "App-prefs:root=General&path=Language_AND_Region")
        case 23://VPN
            jump(url: "App-prefs:root=General&path=VPN")
        case 24://描述文件与设备管理
            jump(url: "App-prefs:root=General&path=ManagedConfigurationList")
        case 25://音乐
            jump(url: "App-prefs:root=MUSIC")
        case 26://备忘录
            jump(url: "App-prefs:root=NOTES")
        case 27: //照片与相机
            jump(url: "App-prefs:root=Photos")
        case 28://还原
            jump(url: "App-prefs:root=General&path=Reset")
        case 29: //Twiter
            jump(url: "App-prefs:root=TWITTER")
        case 30: //Facebook
            jump(url: "App-prefs:root=FACEBOOK")
            
        default:
            break
        }
    }
    
    
    //跳转系统设置页面
    func jump(url:String?){
        
        guard let jumpUrl = URL.init(string: url!) else {
            return
        }
        if UIApplication.shared.canOpenURL(jumpUrl)
        {
            if #available(iOS 10.0, *){
                UIApplication.shared.open(jumpUrl, options: [:], completionHandler: { (success) in
                    if success {
                        print("跳转成功")
                    }
                })
            }else{
                UIApplication.shared.openURL(jumpUrl)
            }
        }
    }
}

















